package forum;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class Forum {
	private boolean logs = false;
	
	private List<AnonymousMessage> messages = Collections.synchronizedList(new ArrayList<>());

	public AnonymousMessage postAnonymousMessage(String message) {
		Date curentDate = new Date(System.currentTimeMillis());
		AnonymousMessage anonymousMessage = new AnonymousMessage(curentDate , message, System.nanoTime());
		this.messages.add(anonymousMessage);
		this.printLog(anonymousMessage.toString());
		return anonymousMessage;
	}

	public List<AnonymousMessage> readAnonymousMessages(Date aBeginDate, Date anEndDate) {

		Date beginDate = aBeginDate.before(anEndDate) ? aBeginDate : anEndDate;
		Date endDate = anEndDate.after(aBeginDate) ? anEndDate : aBeginDate;

		Iterator<AnonymousMessage> it = messages.iterator();
		boolean allMessagesRead = false;
		List<AnonymousMessage> selectedMessages = new ArrayList<>();

		while (it.hasNext() && !allMessagesRead) {
			AnonymousMessage message = (AnonymousMessage) it.next();
			Date messageDate = message.getDate();

			if (messageDate.after(beginDate) && messageDate.before(endDate)) {
				selectedMessages.add(message);
			}
			allMessagesRead = messageDate.after(endDate);
		}
		return selectedMessages;
	}

	public void setLogs(boolean logs) {
		this.logs = logs;
	}
	
	private void printLog(String log) {
		if(logs) {
			System.err.println("Forum -> " + log);
		}
	}
}
