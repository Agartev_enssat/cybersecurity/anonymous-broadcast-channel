package forum;

import java.text.SimpleDateFormat;
import java.util.Date;

public class AnonymousMessage {

	private final Date date;
	private final String message;
	private final long nanoPrecision;
	
	private final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyy 'at' HH:mm:ss");
	
	public AnonymousMessage(Date date, String message, long nanoPrecision) {
		this.date = date;
		this.message = message;
		this.nanoPrecision = nanoPrecision;
	}

	public Date getDate() {
		return date;
	}

	public String getMessage() {
		return message;
	}

	public long getNanoPrecision() {
		return nanoPrecision;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		result = prime * result + (int) (nanoPrecision ^ (nanoPrecision >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AnonymousMessage other = (AnonymousMessage) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		if (nanoPrecision != other.nanoPrecision)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AnonymousMessage [date=" + dateFormatter.format(date) + ", message=" + message + ", nanoPrecision=" + nanoPrecision + "]";
	}
	
	
}
