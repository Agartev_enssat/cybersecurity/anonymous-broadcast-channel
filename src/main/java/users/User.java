package users;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import forum.AnonymousMessage;
import forum.Forum;

public class User implements Runnable {
	private boolean logs = false;
	
	private final Forum forum;
	private final Names name;
	private String secret;

	public User(Names name, Forum forum) {
		this.name = name;
		this.forum = forum;
	}

	public void askForSecretGeneration(long timeInSeconds) {
		long beginTime = System.currentTimeMillis();
		long endTime = System.currentTimeMillis() + (long) (timeInSeconds * 1e3);
		System.out.println("New secret generation asked from " + this.name + " for " + timeInSeconds + " seconds ...");
		forum.postAnonymousMessage("Generation of a new secret-" + beginTime + "-" + endTime);
	}

	private List<AnonymousMessage> generateSecret(long startTimestamp, long endTimestamp) {
		Date startDate = new Date(startTimestamp);
		Date endDate = new Date(endTimestamp);
		Date currentDate = new Date(System.currentTimeMillis());
		this.printLog("Generation of the secret");
		Names otherName = this.getName() == Names.ALICE ? Names.BOB : Names.ALICE;
		List<AnonymousMessage> sentMessages = new ArrayList<>();

		boolean timeIsUp = false;
		while (!timeIsUp) {
			if (currentDate.after(startDate)) {
				try {
					// wait between 1 ms and 10 ms
					Thread.sleep((int) (Math.random() * 9 + 1));
					currentDate.setTime(System.currentTimeMillis());

					// if the current date is still before the end time
					if (currentDate.before(endDate)) {
						// create a random bit between 0 and 1
						int b = (int) (Math.random() * 2);
						String stringMessage = b == 0 ? this.name.toString() : otherName.toString();
						// post the message and save it in a local list
						sentMessages.add(this.forum.postAnonymousMessage(stringMessage));
					} else {
						// if the current date is after the end date then the loop stops
						timeIsUp = true;
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		return sentMessages;
	}

	private void extractSecret(Date start, Date end, List<AnonymousMessage> sentMessages) {
		List<AnonymousMessage> readMessages;
		readMessages = this.forum.readAnonymousMessages(start, end);
		String secret = "";

		// iterator over sent messages
		Iterator<AnonymousMessage> sentMessagesIt = sentMessages.iterator();
		AnonymousMessage sentMessage = sentMessagesIt.next();
		for (Iterator<AnonymousMessage> readMessagesIt = readMessages.iterator(); readMessagesIt.hasNext();) {
			AnonymousMessage readMessage = readMessagesIt.next();

			String name = readMessage.getMessage();
			// set b like the other process sent the message
			int b = 0;
			if (name.equals(this.name.toString())) {
				b = 1;
			}
			// if the message has been sent by this process then b is inverted
			if (sentMessage.equals(readMessage)) {
				b = (b + 1) % 2;
				if (sentMessagesIt.hasNext()) {
					sentMessage = sentMessagesIt.next();
				}
			}
			secret += Integer.toString(b);
		}

		this.secret = secret;
	}

	@Override
	public void run() {
		boolean run = true;
		while (run) {
			List<AnonymousMessage> selectedMessages;

			// read last second messages
			long currentMillis = System.currentTimeMillis();
			selectedMessages = this.forum.readAnonymousMessages(new Date(currentMillis - 1000),
					new Date(currentMillis));

			for (AnonymousMessage anonymousMessage : selectedMessages) {
				String message = anonymousMessage.getMessage();
				if (message.contains("Generation of a new secret")) {
					printLog("Message contains \"Generation of a new Secret\"");
					// the posted message contains the begin timestamp and the end timestamp
					String[] splitedMessage = message.split("-");
					long startTimestamp = Long.parseLong(splitedMessage[1]);
					long endTimestamp = Long.parseLong(splitedMessage[2]);

					// generate a secret and store the sent messages in a list
					List<AnonymousMessage> messagesSent;
					messagesSent = this.generateSecret(startTimestamp, endTimestamp);

					// get the secret from the list
					this.extractSecret(new Date(startTimestamp), new Date(endTimestamp), messagesSent);
					run = false;
				}
			}
		}
	}

	public Forum getForum() {
		return forum;
	}

	public Names getName() {
		return name;
	}

	public String getSecret() {
		return secret;
	}

	public void setLogs(boolean logs) {
		this.logs = logs;
	}

	private void printLog(String log) {
		if(logs) {
			System.err.println(this.name + " -> " + log);
		}
	}

}
