import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import forum.Forum;
import users.*;

public class Main {
	static boolean help;
	static boolean printSecret;
	static int timeToGenerate;
	static boolean printLogs;

	public static void main(String[] args) {
		parseArguments(args);

		if (help) {
			return;
		}

		Forum forum = new Forum();
		User bob = new User(Names.BOB, forum);
		User alice = new User(Names.ALICE, forum);

		bob.setLogs(printLogs);
		alice.setLogs(printLogs);
		forum.setLogs(printLogs);

		Thread tBob = new Thread(bob);
		Thread tAlice = new Thread(alice);

		tBob.start();
		tAlice.start();

		// create a random bit to know if Alice or Bob ask for the creation of a secret
		if ((Math.random() * 2) < 1) {
			bob.askForSecretGeneration(timeToGenerate);
		} else {
			alice.askForSecretGeneration(timeToGenerate);
		}

		try {
			tBob.join();
			tAlice.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		String bobSecret = bob.getSecret();
		String aliceSecret = alice.getSecret();
		if (bobSecret.equals(aliceSecret)) {

			System.out.println("Success, Bob and Alice get the same secret");
			if (printSecret) {
				System.out.println("secret : " + aliceSecret);
			}
		} else {
			System.out.println("DIFFERENT SECRET");
			System.out.println("bob length : " + bobSecret.length());
			System.out.println("alice length : " + aliceSecret.length());
		}

	}

	private static void parseArguments(String[] args) {

		try {
			Options options = new Options();
			options.addOption("h", "help", false, "Print this help message");
			options.addOption("s", "secret", false, "Print the secret at the end");
			options.addOption("t", "time", true, "Set the time to generate the secret (1 -> 60 seconds)");
			options.addOption("v", "verbose", false, "Use verbose logging");

			HelpFormatter helpFormatter = new HelpFormatter();
			helpFormatter.setWidth(110);

			CommandLineParser parser = new DefaultParser();
			CommandLine cmd = parser.parse(options, args);

			// help message
			if (cmd.hasOption("h")) {
				helpFormatter.printHelp("AnonymousBroadcastChannel.jar", options);
				help = true;
			} else {
				help = false;
			}

			// time to generate the secret
			String timeString = cmd.getOptionValue("t");
			if (timeString == null) {
				timeToGenerate = 15;
			} else {
				try {
					timeToGenerate = Integer.parseInt(timeString);
				} catch (NumberFormatException e) {
					System.err.println("\'" + timeString + "\'" + " is not a number, default value used");
					timeToGenerate = 15;
				}
				timeToGenerate = timeToGenerate < 1 ? 1 : timeToGenerate;
				timeToGenerate = timeToGenerate > 60 ? 60 : timeToGenerate;
			}

			// print the secret
			if (cmd.hasOption("s")) {
				printSecret = true;
			} else {
				printSecret = false;
			}

			// verbose mode
			if (cmd.hasOption("v")) {
				printLogs = true;
			} else {
				printLogs = false;
			}
		} catch (ParseException e1) {
			System.err.println("Unrecognized option");
			System.err.println("Try option -h or --help to print the help");
		}
	}

}
