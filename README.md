# Anonymous Broadcast Channel

**Secret Generation**

This sample project is a proof of concept of the Anonymous Broadcast Channel using a forum.
It allows to create a secret key by using a public anonymous forum.

No plugin required, just Gradle

## Prerequisites

- Java 11 or newer


# Create the .jar archive

Execute : `gradle build` from the project root folder 

# Run it

In order to run the .jar file using default vaules, simply execute

    `java -jar build/libs/AnonymousBroadcastChannel.jar`

It is possible to pass some parameters as input to the program. To know how to do so, execute 

    `java -jar build/libs/AnonymousBroadcastChannel.jar - h`
